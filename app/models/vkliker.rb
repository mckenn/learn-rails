# vkliker model class
class Vkliker < ActiveRecord::Base
  # set_primary_key 'id'
  attr_reader :name, :id, :on
  has_no_table
  column :id, :integer
  column :on, :boolean
  column :name, :string
  validates_presence_of :id

  def initialize( id , name = 'Mr. Smith' , on = false)
    @id = id
    @name = name
    @on = on
  end

end
